package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
)

// InitConfig needs to initialize viper and get vars from environment
func InitConfig() {
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		logrus.Fatal("Config parsing error: ", err)
	}


	// Database config
	if e := os.Getenv("AUTH_PORT"); e != "" {
		viper.Set("port", ":" + e)
	}

	if e := os.Getenv("AUTH_DB_HOST"); e != "" {
		viper.Set("db.host", e)
	}

	if e := os.Getenv("AUTH_DB_USER"); e != "" {
		viper.Set("db.user", e)
	}

	if e := os.Getenv("AUTH_DB_PASSWORD"); e != "" {
		viper.Set("db.password", e)
	}
}

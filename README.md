# Микросервис сессий

1. Генерирует токен по `user_id`

2. Проверяет валидность токена по `session_token` и `user_id`

### Возможные ошибки
```
100 - session token invalid

200 - error while generate signed string
201 - Input data doesn't valid

300 - unexpected signing method
301 - error while parsing token
302 - error while parsing user claims from token

400 - input data parse error

```

### Примеры запросов

1. Генерация сессии по user_id

```http request
curl --location --request POST 'http://localhost:8080/session/create' \
--header 'Content-Type: application/json' \
--data-raw '{
	"user_id": 12
}'
```

```json
{
	"status_code": 0,
	"response": {
		"session_access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzExODg4NDEsInN1YiI6IjEyIn0.cPmLYZfzkszWU2kKNIIro51mey_0rwtD3k8wykOjunU"
	}
}
```

2. Проверка сессии на валидность

```http request 
curl --location --request POST 'http://localhost:8080/session/check' \
--header 'Content-Type: application/json' \
--data-raw '{
	"user_id": 11,
	"session_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzExODg4NDEsInN1YiI6IjEyIn0.cPmLYZfzkszWU2kKNIIro51mey_0rwtD3k8wykOjunU"
}'
```

```json
{
	"status_code": 0,
	"response": {
		"status": true
	}
}
```

package usecase

import (
	"github.com/golang-jwt/jwt"
	"github.com/spf13/viper"
	"session_service/internal/domain"
	"strconv"
	"time"
)

type SessionUsecase struct {

}

func NewSessionUsecase() *SessionUsecase {
	return &SessionUsecase{}
}

func (su *SessionUsecase) Create(userID int) (*domain.Session, *domain.SessionError) {
	sessionToken, err := su.GenerateJWTToken(userID)
	if err != nil {
		return nil, err
	}

	return &domain.Session{SessionAccessToken: sessionToken}, nil
}

func (su *SessionUsecase) Check(sessionToken string, userID int) (bool, *domain.SessionError) {
	userIDFromToken, err := su.CheckJWTToken(sessionToken)
	if err != nil {
		return false, err
	}

	if userID != userIDFromToken {
		return false, domain.SessionTokenInvalidError
	}

	return true, nil
}

// GenerateJWTToken method, that returns prepared JWT token
func (su *SessionUsecase) GenerateJWTToken(userID int) (string, *domain.SessionError) {
	// Create a new token object
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		ExpiresAt: time.Now().Add(1 * time.Hour).Unix(),
		Subject:   strconv.Itoa(userID),
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString([]byte(viper.GetString("jwt_secret")))
	if err != nil {
		return "", domain.NewSessionError(200, "error while generate signed string: " + err.Error())
	}

	return tokenString, nil
}

// CheckJWTToken method, that validate JWT token and returns userID
func (su *SessionUsecase) CheckJWTToken(token string) (int, *domain.SessionError) {
	t, err := jwt.Parse(token, func(token *jwt.Token) (i interface{}, err error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, domain.UnexpectedSigningMethodError
		}

		return []byte(viper.GetString("jwt_secret")), nil
	})
	if err != nil {
		return 0, domain.NewSessionError(301, "error while parsing token: " + err.Error())
	}

	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok {
		return 0, domain.UserClaimsParseError
	}

	userID, _ := strconv.Atoi(claims["sub"].(string))
	return userID, nil
}
package api

import (
	"encoding/json"
	"net/http"
	"session_service/internal/domain"
)

type CreateSessionInput struct {
	UserID int `json:"user_id" valid:"required,numeric"`
}

type CheckSessionInput struct {
	UserID int          `json:"user_id" valid:"required,numeric"`
	SessionToken string `json:"session_token" valid:"required"`
}

type SuccessResponse struct {
	StatusCode int         `json:"status_code"`
	Response   interface{} `json:"response"`
}

type CreateSessionResponse struct {
	SessionAccessToken string `json:"session_access_token"`
}

type CheckSessionResponse struct {
	Status bool `json:"status"`
}

type ErrorResponse struct {
	StatusCode   int    `json:"status_code"`
	ErrorMessage string `json:"error_message"`
}

func NewSuccessResponse(w http.ResponseWriter, response interface{}) {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(SuccessResponse{
		StatusCode:   0,
		Response: response,
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func NewErrorResponse(w http.ResponseWriter, sessionError *domain.SessionError) {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(ErrorResponse{
		StatusCode:   sessionError.StatusCode,
		ErrorMessage: sessionError.ErrorMessage,
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
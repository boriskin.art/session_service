package api

import (
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"net/http"
	"session_service/internal/domain"
	"session_service/internal/usecase"
)

type SessionHandler struct {
	sessionUsecase *usecase.SessionUsecase
}

func NewSessionHandler(router *mux.Router, sessionUsecase *usecase.SessionUsecase) {
	handler := &SessionHandler{sessionUsecase: sessionUsecase}

	router.HandleFunc("/session/create", handler.Create).Methods("POST")
	router.HandleFunc("/session/check", handler.Check).Methods("POST")
}

func (sh *SessionHandler) Create(w http.ResponseWriter, r *http.Request) {
	createSessionInput := CreateSessionInput{}

	err := json.NewDecoder(r.Body).Decode(&createSessionInput)
	if err != nil {
		NewErrorResponse(w, domain.InputDataParseError)
		return
	}

	_, err = govalidator.ValidateStruct(createSessionInput)
	if err != nil {
		NewErrorResponse(w, domain.NewSessionError(201, "Input data doesn't valid: " + err.Error()))
		return
	}

	session, e := sh.sessionUsecase.Create(createSessionInput.UserID)
	if e != nil {
		NewErrorResponse(w, e)
		return
	}

	NewSuccessResponse(w, CreateSessionResponse{SessionAccessToken: session.SessionAccessToken})
}

func (sh *SessionHandler) Check(w http.ResponseWriter, r *http.Request) {
	checkSessionInput := CheckSessionInput{}

	err := json.NewDecoder(r.Body).Decode(&checkSessionInput)
	if err != nil {
		NewErrorResponse(w, domain.InputDataParseError)
		return
	}

	_, err = govalidator.ValidateStruct(checkSessionInput)
	if err != nil {
		NewErrorResponse(w, domain.NewSessionError(201, "Input data doesn't valid: " + err.Error()))
		return
	}

	isOk, e := sh.sessionUsecase.Check(checkSessionInput.SessionToken, checkSessionInput.UserID)
	if e != nil {
		NewErrorResponse(w, e)
		return
	}

	NewSuccessResponse(w, CheckSessionResponse{Status: isOk})
}
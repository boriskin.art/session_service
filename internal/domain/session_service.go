package domain

type Session struct {
	SessionAccessToken  string
}

type SessionUsecase interface {
	Create(userID int) (*Session, *SessionError)
	Check(sessionToken string, userID int) (bool, *SessionError)
	GenerateJWTToken(userID int) (string, *SessionError)
	CheckJWTToken(token string) (int, *SessionError)
}
package domain

import "fmt"

type SessionError struct {
	StatusCode   int
	ErrorMessage string
}

func NewSessionError(statusCode int, errorMessage string) *SessionError {
	return &SessionError{
		StatusCode:   statusCode,
		ErrorMessage: errorMessage,
	}
}

func (se *SessionError) Error() string {
	return fmt.Sprintf("[Status code: %v] Error: %v\n", se.StatusCode, se.ErrorMessage)
}

var (
	SessionTokenInvalidError     = NewSessionError(100, "session token invalid")

	UnexpectedSigningMethodError = NewSessionError(300, "unexpected signing method")
	UserClaimsParseError         = NewSessionError(302, "error while parsing user claims from token")

	InputDataParseError          = NewSessionError(400, "input data parse error")
)
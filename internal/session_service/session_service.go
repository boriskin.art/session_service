package session_service

import (
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"session_service/config"
	"session_service/internal/delivery/api"
	"session_service/internal/usecase"
	"time"
)

func Run() {
	config.InitConfig()

	router := mux.NewRouter()

	sessionUsecase := usecase.NewSessionUsecase()
	api.NewSessionHandler(router, sessionUsecase)

	server := http.Server{
		Addr:         viper.GetString("port"),
		Handler:      router,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	logrus.Info("Starting server...")

	if err := server.ListenAndServe(); err != nil {
		logrus.Fatal("Server error: ", err)
	}
}